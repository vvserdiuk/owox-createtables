import com.google.api.services.bigquery.Bigquery;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.bigquery.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.stream.IntStream;

/**
 * @author vvserdiuk on 10.11.17.
 */
public class TablesCreatorService {

    private final static String FIELD_HIT_ID = "hitId";
    private final static String FIELD_USER_ID = "userId";

    /**
     * Creates N tables in given project and dataset with name like prefix[N]
     *
     * @param bigQuery BigQuery object with predefined credentials and projectId
     * @param datasetId dataset in which to create a table
     * @param prefix prefix of created table
     * @param count how many tables to create and also suffix of table name(N)
     */
    public void createTables(BigQuery bigQuery, String datasetId, String prefix, int count) throws IOException {
        Dataset dataset = bigQuery.getDataset(datasetId);
        Schema schema = Schema.of(
                Field.of(FIELD_HIT_ID, LegacySQLTypeName.STRING),
                Field.of(FIELD_USER_ID, LegacySQLTypeName.STRING)
        );

        IntStream.range(0, count)
                .forEach(n -> {
                    String tableName = prefix + n;

                    StandardTableDefinition definition = StandardTableDefinition.newBuilder()
                            .setSchema(schema)
                            .setTimePartitioning(TimePartitioning.of(TimePartitioning.Type.DAY))
                            .build();

                    dataset.create(tableName, definition);
                });

        bigQuery.update(dataset);
    }
}
