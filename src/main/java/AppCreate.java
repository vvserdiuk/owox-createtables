import com.google.auth.oauth2.GoogleCredentials;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.bigquery.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.stream.IntStream;

/**
 * @author vvserdiuk on 10.11.17.
 */
public class AppCreate {

    public static void main(String[] args) throws IOException {
        String projectId = args[0];
        String datasetId = args[1];
        String prefix = args[2];
        Integer count = Integer.valueOf(args[3]);

        BigQuery bigquery;
        if (args.length == 5) {
            String keyPath = args[4];
            bigquery = initBigQuery(projectId, keyPath);
        } else {
            bigquery = initBigQuery(projectId);
        }

        TablesCreatorService tablesCreatorService = new TablesCreatorService();
        tablesCreatorService.createTables(bigquery, datasetId, prefix, count);
    }


    /**
     * Init BigQuery by its projectId. Uses default credentials from environment variable
     */
    private static BigQuery initBigQuery(String projectId) throws IOException {
        GoogleCredentials credentials = GoogleCredentials.getApplicationDefault();

        return BigQueryOptions.newBuilder()
                .setProjectId(projectId)
                .setCredentials(credentials).build().getService();
    }

    /**
     * Init BigQuery by its projectId. Gets credentials from keyPath
     */
    private static BigQuery initBigQuery(String projectId, String keyPath) throws IOException {
        GoogleCredentials credentials;
        File credentialsPath = new File(keyPath);

        try (FileInputStream serviceAccountStream = new FileInputStream(credentialsPath)) {
            credentials = ServiceAccountCredentials.fromStream(serviceAccountStream);
        }

        return BigQueryOptions.newBuilder()
                .setProjectId(projectId)
                .setCredentials(credentials).build().getService();
    }
}
