import com.google.api.gax.paging.Page;
import com.google.api.services.bigquery.Bigquery;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.bigquery.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.junit.Assert.*;

/**
 * @author vvserdiuk on 10.11.17.
 */
public class TablesCreatorServiceTest {

    private final static String FIELD_HIT_ID  = "hitId";
    private final static String FIELD_USER_ID = "userId";
    private final static String PROJECT_ID    = "vvserdiuk-test";
    private final static String DATASET_ID    = "test";

    TablesCreatorService creatorService = new TablesCreatorService();

    BigQuery bigQuery;
    StandardTableDefinition definition;
    Schema schema;


    @Before
    public void setUp() throws Exception {
        GoogleCredentials credentials;
        File credentialsPath = new File(this.getClass().getClassLoader().getResource("vvserdiuk-test-credentials.json").toURI());

        try (FileInputStream serviceAccountStream = new FileInputStream(credentialsPath)) {
            credentials = ServiceAccountCredentials.fromStream(serviceAccountStream);
        }

        bigQuery =  BigQueryOptions.newBuilder()
                .setProjectId(PROJECT_ID)
                .setCredentials(credentials).build().getService();
        schema = Schema.of(
                Field.of(FIELD_HIT_ID, LegacySQLTypeName.STRING),
                Field.of(FIELD_USER_ID, LegacySQLTypeName.STRING)
        );
        definition = StandardTableDefinition.newBuilder()
                .setSchema(schema)
                .setTimePartitioning(TimePartitioning.of(TimePartitioning.Type.DAY))
                .build();
    }

    @After
    public void tearDown() throws Exception {
        bigQuery.delete(TableId.of(PROJECT_ID, DATASET_ID, "aaa0"));
        bigQuery.delete(TableId.of(PROJECT_ID, DATASET_ID, "aaa1"));
        bigQuery.delete(TableId.of(PROJECT_ID, DATASET_ID, "aaa2"));
    }

    @Test
    public void successfulCreationTest() throws Exception {
        String prefix = "aaa";
        Integer count = 3;

        TableId aaa0 = Table.of(TableId.of(PROJECT_ID, DATASET_ID, "aaa0"), definition).getTableId();
        TableId aaa1 = Table.of(TableId.of(PROJECT_ID, DATASET_ID, "aaa1"), definition).getTableId();
        TableId aaa2 = Table.of(TableId.of(PROJECT_ID, DATASET_ID, "aaa2"), definition).getTableId();
        List<TableId> expectedTables = new ArrayList();
        expectedTables.add(aaa0);
        expectedTables.add(aaa1);
        expectedTables.add(aaa2);

        creatorService.createTables(bigQuery, DATASET_ID, prefix, count);


        final Iterable<Table> tables = bigQuery.listTables(DATASET_ID).getValues();
        final List<TableId> actual = StreamSupport.stream(tables.spliterator(), false)
                .map(TableInfo::getTableId)
                .collect(Collectors.toList());

        assertThat(actual, hasItems(aaa0, aaa1, aaa2));
    }

    @Test
    public void createdTablesHasRightStructureTest() throws Exception {
        String prefix = "aaa";
        Integer count = 3;

        creatorService.createTables(bigQuery, DATASET_ID, prefix, count);
        final Iterable<Table> tables = bigQuery.listTables(DATASET_ID).getValues();

        StreamSupport.stream(tables.spliterator(), false)
                .forEach( t -> {
                    Table table = bigQuery.getTable(t.getTableId());
                    assertEquals(schema, table.getDefinition().getSchema());
                });
    }
}